/* Copyright (c) 2008-11 lvqcl.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */ 

#include "stdafx.h"

void dsp_rate::init()
{
	t_dsp_rate_params params;
	params.get_rateconfig(cfg_);

	sample_rate_ = 0;
	out_rate_ = 0;
	channel_count_ = 0;
	channel_map_ = 0;

	resamplers_ = 0;

	out_buffer_size_ = 0;
	out_buffer_ = 0;
}

dsp_rate::dsp_rate()
{
	init();
}

dsp_rate::dsp_rate(const t_dsp_rate_params& params)
{
	init();
	params.get_rateconfig(cfg_);
}

/*dsp_rate::dsp_rate(const dsp_preset& p_data)
{
	init();
	set_data(p_data);
}*/

bool dsp_rate::set_data(const dsp_preset & p_data)
{
	t_dsp_rate_params params;
	if (!params.set_data(p_data)) return false;
	params.get_rateconfig(cfg_);
	return true;
}

dsp_rate::~dsp_rate()
{
	if (resamplers_)
	{
		for (int i = 0; i < channel_count_; ++i)
			resampler_delete(resamplers_[i]);
		free(resamplers_);
	}
	if (out_buffer_)
		free(out_buffer_);
}

void dsp_rate::on_endoftrack(abort_callback & p_abort) { flushwrite(false); }

void dsp_rate::on_endofplayback(abort_callback & p_abort) { flushwrite(true); }

void dsp_rate::reinit(unsigned sample_rate, unsigned channel_count, unsigned channel_map)
{
	if (channel_count_ != channel_count)
	{
		if (!resamplers_)
			resamplers_ = (void**) calloc(sizeof(void*), channel_count_);
		for (int i = 0; i < channel_count_; ++i)
			resampler_clear(resamplers_[i]);

		if (channel_count_ > channel_count)
		{
			for (int i = channel_count; i < channel_count_; ++i)
				resampler_delete(resamplers_[i]);
		}
	}

	out_rate_ = cfg_.realrate(sample_rate);

	ratio_ = (float)sample_rate / (float)out_rate_;

	void * resamplers_temp = realloc(resamplers_, sizeof(void*) * channel_count);
	if (!resamplers_temp)
		throw std::bad_alloc();
	resamplers_ = (void**) resamplers_temp;

	for (int i = channel_count_; i < channel_count; ++i)
		resamplers_[i] = resampler_create();

	for (int i = 0; i < channel_count; ++i)
	{
		resampler_set_quality(resamplers_[i], cfg_.quality);
		resampler_set_rate(resamplers_[i], ratio_);
	}

	channel_count_ = channel_count; channel_map_ = channel_map; sample_rate_ = sample_rate;

	samples_in_ = 0; samples_out_ = 0;
}

bool dsp_rate::on_chunk(audio_chunk * chunk, abort_callback & p_abort)
{
	unsigned channel_count = chunk->get_channels();
	unsigned channel_map = chunk->get_channel_config();
	unsigned sample_rate = chunk->get_sample_rate();
	t_size sample_count = chunk->get_sample_count();
	t_size estimated_output_count;
	audio_sample * current = chunk->get_data();
	audio_sample * current_out;

	if ((channel_count_ != channel_count) || (channel_map_ != channel_map) || (sample_rate_ != sample_rate))
	{	// number of channels or samplerate has changed - reinitialize
		flushwrite(true); //here channel_count_, channel_map_ and sample_rate_ must have old values
		//rate_state_ == NULL here
		if (cfg_.is_no_resample(sample_rate)) return true;
		reinit(sample_rate, channel_count, channel_map);
	}

	estimated_output_count = (t_size)(sample_count / ratio_) + 128;

	if (out_buffer_size_ < estimated_output_count)
	{
		out_buffer_size_ = estimated_output_count;
		out_buffer_ = (audio_sample *)realloc(out_buffer_, sizeof(audio_sample) * channel_count * estimated_output_count);
	}

	current_out = out_buffer_;

	while( ( sample_count && resampler_get_free_count( resamplers_[0] ) ) || resampler_get_sample_count( resamplers_[0] ) )
	{
		while ( sample_count && resampler_get_free_count( resamplers_[0] ) )
		{
			for (int i = 0; i < channel_count; ++i)
				resampler_write_sample_float(resamplers_[i], current[i]);
			current += channel_count;
			--sample_count;
			++samples_in_;
		}

		while (resampler_get_sample_count(resamplers_[0]))
		{
			for (int i = 0; i < channel_count; ++i)
			{
				current_out[i] = resampler_get_sample_float(resamplers_[i]);
				resampler_remove_sample(resamplers_[i], 1);
			}
			current_out += channel_count;
			++samples_out_;
		}
	}

	audio_chunk * out = insert_chunk(1024 * channel_count_);
	out->set_data(out_buffer_, (current_out - out_buffer_) / channel_count, channel_count, out_rate_);

	return false;
}

void dsp_rate::flush()
{
	return;
}

void dsp_rate::flushwrite(bool close)
{
	if ( close && channel_count_ && sample_rate_ )
	{
		unsigned latency_samples = resampler_latency( resamplers_[0] );
		audio_chunk_impl temp_chunk;
		temp_chunk.pad_with_silence_ex( latency_samples, channel_count_, sample_rate_);
		temp_chunk.set_channels( channel_count_, channel_map_ );
		on_chunk( &temp_chunk, abort_callback_dummy() );
	}
	return;
}

double dsp_rate::get_latency()
{
	if (sample_rate_ && out_rate_)
		return ((double(samples_in_) / double(ratio_)) - double(samples_out_)) / double(out_rate_);
	else return 0;
}
